template <typename T>
void shellsort(T* tab, int size)
{
    for(int gap = size / 2; gap > 0; gap /= 2){
        for(int i = gap; i < size; ++i){
            int temp = tab[i];
            int k;
            for(k = i; (k >= gap && temp < tab[k - gap]); k -= gap){
                tab[k] = tab[k - gap];
            }
            tab[k] = temp;
        }
    }

}
