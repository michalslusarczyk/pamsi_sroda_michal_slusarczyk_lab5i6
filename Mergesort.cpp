template<typename T>
void merge(T* tab, int startBound, int middleBound, int endBound){
    int tempSizeA = middleBound-startBound+1;
    int tempA[tempSizeA];

    for(int i = 0; i < tempSizeA; ++i){
        tempA[i] = tab[startBound+i];
    }

    int tempSizeB = endBound-middleBound;
    int tempB[tempSizeB];

    for(int i = 0; i < tempSizeB; ++i){
        tempB[i] = tab[middleBound+i+1];
    }

    int i = 0;
    int j = 0;
    int k = startBound;

    while(i < tempSizeA && j < tempSizeB){
        if(tempA[i] < tempB[j]){
            tab[k] = tempA[i];
            ++i;
        }else{
            tab[k] = tempB[j];
            ++j;
        }
        ++k;
    }

    while(i < tempSizeA){
        tab[k] = tempA[i];
        ++i;
        ++k;
    }

    while(j < tempSizeB){
        tab[k] = tempB[j];
        ++j;
        ++k;
    }
}

template<typename T>
void mergesort(T* tab, int startBound, int endBound){
    if(startBound < endBound){
            int middle = (startBound + endBound)/2;
            //std::cout<<"Middle: "<<middle<<"\n";
            mergesort(tab, startBound, middle);
            mergesort(tab, middle+1, endBound);
            merge(tab, startBound, middle, endBound);
    }
}
