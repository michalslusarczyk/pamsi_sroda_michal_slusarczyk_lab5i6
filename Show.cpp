#include <iostream>
#include <cstdlib>

#include "Quicksort.cpp"
#include "Mergesort.cpp"
#include "Shellsort.cpp"

using namespace std;

template<typename T>
void printArray(T* tab, int size){
    int i = 0;
    while(i<size){
        cout<<tab[i]<<" ";
        ++i;
    }
    cout<<"\n";
}

template<typename T>
bool validate(T* tab, int size){
    for(int i=0; i<size-1; i++){
        if(tab[i]>tab[i+1]){
            return false;
        }
    }
    return true;
}


int main(){


    int tab[] = {-9, 10, 1, 0, 221, 34, 55, 67, 11, -84};
    printArray(tab, 10);
    quicksort(tab, 0, 9);
    printArray(tab, 10);

    int tab2[] = {-9, 10, 1, 0, 221, 34, 55, 67, 11, -84};
    printArray(tab2, 10);
    mergesort(tab2, 0, 9);
    printArray(tab2, 10);

    int tab3[] = {-9, 10, 1, 0, 221, 34, 55, 67, 11, -84};
    printArray(tab3, 10);
    shellsort(tab3, 10);
    printArray(tab3, 10);


    cout<<"Quicksort - Czy dobrze?: "<<validate(tab, 10)<<"\n";
    cout<<"Mergesort - Czy dobrze?: "<<validate(tab2, 10)<<"\n";
    cout<<"Shellsort - Czy dobrze?: "<<validate(tab3, 10)<<"\n";

    return 0;

}
