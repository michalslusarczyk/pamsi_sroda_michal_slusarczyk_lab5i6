#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <windows.h>
#include <sstream>

#include "Quicksort.cpp"
#include "Mergesort.cpp"
#include "Shellsort.cpp"

using namespace std;

template <typename T>
string to_string ( T Number )
{
	stringstream ss;
	ss << Number;
	return ss.str();
}

LARGE_INTEGER startTimer()
{
LARGE_INTEGER start;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&start);
 SetThreadAffinityMask(GetCurrentThread(), oldmask);
 return start;
}
LARGE_INTEGER endTimer()
{
LARGE_INTEGER stop;
DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
QueryPerformanceCounter(&stop);
SetThreadAffinityMask(GetCurrentThread(), oldmask);
return stop;
}

template<typename T>
void printTable(T* tab, int size){
    int i = 0;
    while(i<size){
        cout<<tab[i]<<" ";
        ++i;
    }
    cout<<"\n";
}

template<typename T>
bool validate(T* tab, int size){
    for(int i=0; i<size-1; i++){
        if(tab[i]>tab[i+1]){
            return false;
        }
    }
    return true;
}


int main(){
    string* lines = new string[800];

    int sizes[] = {10000, 50000, 100000, 500000, 1000000};
    double percentSort[] = {0, 0.25, 0.5, 0.75, 0.95, 0.99, 0.997, 1};
    int percentSortSize = 8;
    int sizesSize = 5;

    int o = 0;
    for(int i = 0; i < percentSortSize; ++i){ //przez 5 roznych posortowan
        int percent = (int)(percentSort[i]*1000);
        for(int s = 0; s < 5; ++s){ //przez 5 wielkosci
            int size = sizes[s];
            for(int k = 0; k < 20; ++k){ //przez 20 sztuk kazdej wielkosci
                std::ifstream plik;
                string path = "out_"+to_string(size)+"_"+to_string(k)+"_"+to_string(percent);
                plik.open(path);
                int* arr;
                arr = new int[size];
                int i = 0;
                while(i<size){
                    plik>>arr[i];
                    i++;
                }
                plik.close();
                LARGE_INTEGER performanceCountStart,performanceCountEnd;
                performanceCountStart = startTimer();
                shellsort(arr, size);
                performanceCountEnd = endTimer();
                double tm = performanceCountEnd.QuadPart - performanceCountStart.QuadPart;
                string result = "shellsort,";
                result+=to_string(size);
                result+=",";
                result+=to_string(percent);
                result+=",";
                result+=to_string(tm);
                lines[o] = result;
                o++;
                cout<<validate(arr, size)<<"\n";
                delete arr;
            }
        }

    }


    std::ofstream output;
    output.open("output_shellsort.csv");

    for(int a = 0; a < 800; ++a){
        output<<lines[a]<<"\n";
    }

    output.close();

    return 0;

}

