import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Generator {

	public static void main(String[] args) throws FileNotFoundException{			
		
		int[] sizes = {10000, 50000, 100000, 500000, 1000000};
		double[] percentSort = {0, 0.25, 0.5, 0.75, 0.95, 0.99, 0.997, 1};
		
		for(int s=0; s<sizes.length; ++s){ //DLA KA�DEGO ROZMIARU
			int size = sizes[s];
			for(int i=0; i<20; ++i){ //ILOSC TABLIC DANEGO ROZMIARU			
				ArrayList<Integer> list = getRandomArray(size);
				Collections.sort(list);
				for(int k=0; k<percentSort.length; ++k){ //TABLICE W ROZNYM STOPNIU POSORTOWANE
					Collections.sort(list);
					PrintWriter plik = new PrintWriter("out_"+size+"_"+i+"_"+(int)(percentSort[k]*1000));
					List<Integer> listA = list.subList(0, (int) (percentSort[k]*size));
					List<Integer> listB = list.subList((int) (percentSort[k]*size), size);
					
					Collections.shuffle(listB);
					
					ArrayList<Integer> list2 = new ArrayList<Integer>();
					
					list2.addAll(listA);
					list2.addAll(listB);
					
					if(percentSort[k] == 1){
						writeFileInverted(list2, plik);
					}else{
						writeFile(list2, plik);
					}
					plik.close();
				}
				
			}
		}
		
		
	}
	
	public static void writeFileInverted(ArrayList<Integer> list, PrintWriter plik){
		for(int i=list.size()-1; i>=0; --i){
			plik.println(list.get(i));
		}
	}
	
	public static void writeFile(ArrayList<Integer> list, PrintWriter plik){
		int i;
		for(i=0; i<list.size(); ++i){
			plik.println(list.get(i));
		}
	}
	
	public static void printArray(ArrayList<Integer> list){
		for(int i=0; i<list.size(); ++i){
			System.out.println(list.get(i));
		}	
	}
	
	public static ArrayList<Integer> getRandomArray(int n){
		ArrayList<Integer> list = new ArrayList<Integer>();
		Random rand = new Random(System.currentTimeMillis());
		
		for(int i=0; i<n; ++i){
			list.add(rand.nextInt());
		}
		return list;
	}

}
