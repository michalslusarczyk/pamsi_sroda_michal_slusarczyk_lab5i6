template<typename T>
void swapElements(T* tab, int i, int j){
    T temp = tab[i];
    tab[i] = tab[j];
    tab[j] = temp;
}

template<typename T>
void quicksort(T* tab, int startBound, int endBound)
{
    int i = startBound;
    int j = endBound;
    T pivot = tab[(startBound+endBound)/2];
    while(i<=j){

        while (tab[i] < pivot){
            i++;
        }

        while (tab[j] > pivot){
            j--;
        }

        if(i<=j){
            swapElements(tab, i, j);
            i++;
            j--;
        }
    }
    if(startBound < j){
        quicksort(tab, startBound, j);
    }
    if(i < endBound){
        quicksort(tab, i, endBound);
    }
}

